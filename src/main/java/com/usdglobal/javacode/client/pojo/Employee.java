package com.usdglobal.javacode.client.pojo;
import java.util.ArrayList;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

public class Employee
{
	@JsonPropertyDescription("This is to get and set ID of Employee")
    private int id;

	@JsonPropertyDescription("This is to get and set ID of Employee")
	private String firstName;

	@JsonPropertyDescription("This is to get and set ID of Employee")
	private String lastName;
	
	@JsonPropertyDescription("This is to get and set ID of Employee")
	private ArrayList<String> messages = new ArrayList<String>();
	
	@JsonPropertyDescription("This is to get and set ID of Employee")
	private String statement;
	
	@JsonPropertyDescription("This is to get and set IP address of the Client System")
	private String ipaddress; 
	
	@JsonPropertyDescription("This is to get and set Time from the Client System")
	private String time;
	
	@JsonPropertyDescription("This is to get and set Date from the Current System")
	private String date;
	
	public Employee()
	{
	   
    }
   
   public int getId()
   {
      return id;
   }
   
   public void setId(int id)
   {
      this.id = id;
   }
   
   public String getFirstName()
   {
      return firstName;
   }
   
   public void setFirstName(String firstName)
   {
      this.firstName = firstName;
   }
   
   public String getLastName()
   {
      return lastName;
   }
   
   public void setLastName(String lastName)
   {
      this.lastName = lastName;
   }
   
   public void setmessages(ArrayList<String> message)
   {
	 this.messages=message;
   }
   
   public ArrayList<String> getmessages()
   {
	   return messages;
   }
   
   public void setstatement(String statement)
   {
	   this.statement=statement;
   }
   
   public String getstatement()
   {
	   return statement;
   }
 
   public void setdate(String date)
   {
 	   this.date=date;
 	   
   }
    
   public void settime(String time)
   {
 	   this.time=time;
   }
   public String getdate()
   {
 	   return date;
   }
 	
   public String gettime()
   {
 	   return time;
   }
 
   public void setipaddress(String ipaddress)
   {
 	   this.ipaddress=ipaddress;
   }
    
   public String getipaddress()
   {
 	   return ipaddress;
   } 
 
   @Override
   public String toString()
   {
	 return "Employees [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName +", Statement=" + statement +", IPAddress=" + ipaddress +", date=" + date + ", time=" + time + ", messages= "+messages.toString()+" ]";
	   
   }
}