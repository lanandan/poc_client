package com.usdglobal.javacode.client;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Scanner;
import org.json.JSONObject;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.module.jsonSchema.factories.SchemaFactoryWrapper;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.github.fge.jsonschema.report.ProcessingMessage;
import com.github.fge.jsonschema.report.ProcessingReport;
import com.github.fge.jsonschema.util.JsonLoader;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.usdglobal.javacode.client.pojo.Employee;

public class Clientinteraction {
	
	public static void main(String args[])
	{
	Scanner in = new Scanner(System.in);
	System.out.println("Enter the ID :");
    int id=Integer.parseInt(in.nextLine());
    System.out.println("Enter the First Name:");
    String first_name = in.nextLine();
    System.out.println("Enter the Last Name:");
    String last_name = in.nextLine();
    System.out.println("Enter the string to be checked:");
    String statement = in.nextLine();
    System.out.println("What is your favourite food");
    String food = in.nextLine();
    System.out.println("What is your favourite song:");
    ArrayList<String> personality_profile = new ArrayList<String>();
    String song = in.nextLine();
    personality_profile.add(food);
    personality_profile.add(song);
    String jsonSchema=null;
	String jsonData=null;
	ProcessingReport report = null;
	boolean result = false;
	Employee employee = new Employee();
	Date todaysdate = new Date();
 	Date todaystime=new Date();
 	String ips="";
 	
	try
	{
		//Json Schema generation 
		ObjectMapper mapper = new ObjectMapper();
        SchemaFactoryWrapper visitor = new SchemaFactoryWrapper();
        mapper.acceptJsonFormatVisitor(Employee.class, visitor);
        com.fasterxml.jackson.module.jsonSchema.JsonSchema schema = visitor.finalSchema();
        jsonSchema= mapper.writerWithDefaultPrettyPrinter().writeValueAsString(schema);   
        System.out.println("json original schema: \n"+jsonSchema);		
	}
	catch(Exception e)
	{
		System.out.println(e+"");
	}
	
	try
	{
		//Json Validation with Json Schema
		//input for Authentication by service	
	 	SimpleDateFormat date = new SimpleDateFormat ("MM.dd.yyyy");
	 	SimpleDateFormat time=new SimpleDateFormat("hh:mm:ss a");
	 	todaysdate=new SimpleDateFormat("MM.dd.yyyy").parse(date.format(todaysdate));
	 	todaystime=new SimpleDateFormat("hh:mm:ss a").parse(time.format(todaysdate));
	 	InetAddress ip=InetAddress.getLocalHost();
		ips = InetAddress.getLocalHost().getHostAddress();
		ObjectMapper mapper = new ObjectMapper();
	
		employee.setId(id);
		employee.setFirstName(first_name);
	    employee.setLastName(last_name);
	    employee.setstatement(statement);
	    employee.setmessages(personality_profile);
	    employee.setdate(todaysdate+"");
	    employee.settime(todaystime+"");
	    employee.setipaddress(ips+"");
	    
	    jsonData= mapper.writerWithDefaultPrettyPrinter().writeValueAsString(employee);  	
        System.out.println("Json Schema of data :"+jsonData);
        
        JsonNode schemaNode = JsonLoader.fromString(jsonSchema);
        JsonNode data = JsonLoader.fromString(jsonData);         
        JsonSchemaFactory factory = JsonSchemaFactory.byDefault(); 
        JsonSchema schema = factory.getJsonSchema(schemaNode);
        report = schema.validate(data);
        	if (report != null) {
            Iterator<ProcessingMessage> iter = report.iterator();
    
            while (iter.hasNext()) {
                ProcessingMessage pm = iter.next();
                System.out.println("Processing Message: "+pm.getMessage());
            }    	
    	        
    	       result = report.isSuccess();//gets the result after validation
    	       
    	       if(result==true)//if schema validation gets succeed
    	       {    	    	
    	    	try {
    	    	//calling restful api to submit or post the value for calculation
    	    	
    	    	Client client = Client.create();
    	    	WebResource webResource = client.resource("http://192.168.1.67:8080/POC_server/rest/serverservice/stringcalculation");
    	    	ClientResponse response = webResource.accept("application/json").type("application/json").post(ClientResponse.class,employee);
                
    	    	if (response.getStatus() != 200) {//server response not get succeed
                throw new RuntimeException("Failed : HTTP error code : "+ response.getStatus());
                }
    	    	else{//server response gets succeed
    	    	
    	    		try {
    	    			//to get service from restful
    	    			URL url = new URL("http://192.168.1.67:8080/POC_server/rest/serverservice/get");
    	    			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    	    			conn.setRequestMethod("GET");
    	    			conn.setRequestProperty("Accept", "application/json");
    	    			
    	    	    	if (conn.getResponseCode() != 200) {
    	    				throw new RuntimeException("Failed : HTTP error code : "+ conn.getResponseCode());
    	    			}
    	    	    	else
    	    	    	{
    	    	 		BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
    	    			JSONObject json=new JSONObject(br.readLine());
    	    			
    	    			System.out.println("Service from Server");
    	                System.out.println();
    	                System.out.println( "Your ID: "+json.get("id"));
    	                System.out.println("Your First Name: "+json.get("firstName"));
    	                System.out.println("Your Last Name: "+json.get("lastName"));
    	                System.out.println("Your Given Statement: "+json.get("statement"));
    	                System.out.println("The Number of Upper Case Letters : "+json.get("uppercase"));
    	                System.out.println("The Number of Lower Case Letters : "+json.get("lowercase"));
    	                System.out.println("The Number of Vowels : "+json.get("vowels"));
    	                System.out.println("The Number of White Spaces : "+json.get("spaces"));
    	                org.json.JSONArray jsonArray = json.getJSONArray("messages");
    	                
    	                for (int i = 0; i < jsonArray.length(); i++) {
    	                	
    	                    System.out.println(jsonArray.get(i));
    	                }//end of for for parsing json array
    	    	    	
    	    	    	}  
    	    		  } 
    	    			catch (Exception e)
    	    		  {
    	    	 
    	    			e.printStackTrace();
    	    	 
    	    		  } 	
    	    		}//end of successful json array parsing 
    	    	} 
    	    	catch (Exception e) {
      	        
    	    		e.printStackTrace();
       	       
    	    	}
    	       
    	       }//end of schema validation success block
    	       else//If schema validation not gets succeed
    	       {
    	    	   System.out.println("Fail :"+result);
    	       }//end of schema validation failure part
        
        }
                
	}
	catch(Exception e)
	{
		System.out.println(e+"");
	}
	
	   
}
}

